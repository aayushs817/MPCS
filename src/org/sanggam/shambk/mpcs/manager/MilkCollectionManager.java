/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sanggam.shambk.mpcs.bean.MilkCollectionBean;

/**
 *
 * @author beschi
 */
public class MilkCollectionManager {
    DatabaseManager connection;

    public List<MilkCollectionBean> getMilkData(){
        connection = new DatabaseManager();
        List<MilkCollectionBean> listBean = new ArrayList<MilkCollectionBean>();
        MilkCollectionBean bean = null;
        Date toDate = new Date();
        int month = toDate.getMonth();
        int year = toDate.getYear();
        String sql = "SELECT  m.mid, c.regno, c.name, m.amount, m.quantity, m.collection_date, m.slot FROM milkcollection m, customer c WHERE c.regno = m.customer_no ORDER BY m.collection_date DESC;";
        ResultSet resultSet = connection.getRecords(sql);
        try {
            while (resultSet.next()) {
                bean = new MilkCollectionBean();
                bean.setId(resultSet.getInt(1));
                bean.setRegNo(resultSet.getInt(2));
                bean.setName(resultSet.getString(3));
                bean.setAmount(resultSet.getDouble(4));
                bean.setQuantity(resultSet.getDouble(5));
                bean.setMdate(resultSet.getObject(6));
                String t = resultSet.getInt(7) == 1?"Morning":"Evening";
                bean.setTime(t);
                listBean.add(bean);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MilkCollectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.disconnect();
        return listBean;
    }


    public int addMilkInfo(MilkCollectionBean bean){
        connection = new DatabaseManager();
        int time = bean.getTime()=="Morning"?1:2;
        String sql ="INSERT INTO milkcollection " +
                    "(customer_no,amount,quantity,collection_date,slot) VALUES (" +
                    bean.getRegNo()+","+bean.getAmount()+","+bean.getQuantity()+
                    ",'"+bean.getMdate()+"',"+time+"); ";

        int status = connection.addRecord(sql);
        connection.disconnect();
        return status;
    }

    public int deleteMilkInfo(int mid){
        connection = new DatabaseManager();
        String sql = "DELETE FROM milkcollection WHERE mid="+mid;
        int status = connection.deleteRecord(sql);
        connection.disconnect();
        return  status;
    }

    public int updateMilkInfo(int mid, MilkCollectionBean bean){
        connection = new DatabaseManager();
        int time = bean.getTime().equalsIgnoreCase("Morning")?1:2;
        String sql ="UPDATE milkcollection " +
                    " SET customer_no =" + bean.getRegNo()+",amount = "+bean.getAmount()+
                    ",quantity="+bean.getQuantity()+", collection_date='"+bean.getMdate()+
                    "',slot ='"+time+"' WHERE mid="+mid+";";
       System.out.println(sql);
        int status = connection.updateRecord(sql);
        connection.disconnect();
        return status;
    }

    public MilkCollectionBean getMilkCollectionById(int mid){
        connection = new DatabaseManager();
        MilkCollectionBean bean = null;
        String sql = "SELECT  mid, customer_no, amount, quantity, collection_date, slot FROM milkcollection WHERE mid ="+mid+";";
        ResultSet resultSet = connection.getRecords(sql);
        try {
            while (resultSet.next()) {
                bean = new MilkCollectionBean();
                bean.setId(resultSet.getInt(1));
                bean.setRegNo(resultSet.getInt(2));
                bean.setAmount(resultSet.getDouble(3));
                bean.setQuantity(resultSet.getDouble(4));
                bean.setMdate(resultSet.getObject(5));
                String t = resultSet.getInt(6) == 1?"Morning":"Evening";
                bean.setTime(t);
                }
        } catch (SQLException ex) {
            Logger.getLogger(MilkCollectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        connection.disconnect();
        return bean;
    }

//    public static void main(String[] args){
//        MilkCollectionManager manager = new MilkCollectionManager();
//        List<MilkCollectionBean> list = manager.getMilkData();
//        for(MilkCollectionBean bean : list){
//            System.out.println(bean.getId());
//        }
//    }
}
