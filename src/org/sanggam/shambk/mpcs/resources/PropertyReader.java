package org.sanggam.shambk.mpcs.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertyReader {

        private static ResourceBundle bdl;

        private String getClassName()
           {
              String thisClassName;

              //Build a string with executing class's name
              thisClassName = this.getClass().getName();
              thisClassName = thisClassName.substring(thisClassName.lastIndexOf(".") + 1,thisClassName.length());
              thisClassName += ".class";  //this is the name of the bytecode file that is executing

              return thisClassName;
           }

        private String getLocalDirName()
        {
          String localDirName;

          //Use that name to get a URL to the directory we are executing in
          java.net.URL myURL = this.getClass().getResource(getClassName());  //Open a URL to the our .class file

          //Clean up the URL and make a String with absolute path name
          localDirName = myURL.getPath();  //Strip path to URL object out
          localDirName = myURL.getPath().replaceAll("%20", " ");  //change %20 chars to spaces

          //Get the current execution directory
          localDirName = localDirName.substring(0,localDirName.lastIndexOf("/"));  //clean off the file name

          return localDirName;
        }

	private PropertyReader() {
            try {
                 bdl = new PropertyResourceBundle(new FileInputStream(getLocalDirName() + "/mps.properties"));
                 } catch (IOException ex) {
              //  Logger.getLogger(PropertyReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

	public static String getString(String key) {
                new PropertyReader();
		try {
			return bdl.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

}
