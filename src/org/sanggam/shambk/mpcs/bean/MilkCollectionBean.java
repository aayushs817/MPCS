/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.bean;

/**
 *
 * @author beschi
 */
public class MilkCollectionBean {
    private int sno;

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }
    private int id;
    private int regNo;
    private String name;
    private double amount;
    private double quantity;
    private Object mdate;
    private String time;

    public MilkCollectionBean() {
    }

    public MilkCollectionBean(int sno, int id, int rNo, String name,double amt, double qty, Object md, String t) {
        this.sno = sno;
        this.id = id;
        this.regNo = rNo;
        this.name = name;
        this.amount = amt;
        this.quantity = qty;
        this.mdate = md;
        this.time = t;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getMdate() {
        return mdate;
    }

    public void setMdate(Object mdate) {
        this.mdate = mdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getRegNo() {
        return regNo;
    }

    public void setRegNo(int regNo) {
        this.regNo = regNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
