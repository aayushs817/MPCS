/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sanggam.shambk.mpcs.ui.util;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author bosso
 */
public class Screen {
    private static Toolkit toolkit = Toolkit.getDefaultToolkit();

    /**
     *
     * @return Screen in size
     */
    public static Dimension getSize(){
        return toolkit.getScreenSize();
    }

    /**
     *
     * @return Screen with
     */
    public static int getScreenWidth(){
        return (int)toolkit.getScreenSize().getWidth();
    }

    /**
     * 
     * @return
     */
    public static int getScreenHeight(){
        return (int)toolkit.getScreenSize().getHeight();
    }

    public static int xPosition( int with){
        System.out.println(with);
         System.out.println("X"+(getScreenWidth()-with)/2);
        return ((getScreenWidth()-with)/2);
    }

    public static int yPosition(int height){
        System.out.println(height);
        System.out.println("Y"+(getScreenHeight()-height)/2);
        return ((getScreenHeight()-height)/2);
    }

}
