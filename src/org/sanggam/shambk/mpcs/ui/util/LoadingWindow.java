/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * LoadingWindow.java
 *
 * Created on Jul 1, 2010, 4:02:19 PM
 */

package org.sanggam.shambk.mpcs.ui.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;


/**
 *
 * @author bosso
 */
public class LoadingWindow extends javax.swing.JWindow {
   

    /** Creates new form LoadingWindow */
    public LoadingWindow() {
      initComponents();
      
    }
    
   
    private void initComponents() {
       	        JWindow loadingWindow = new JWindow();
                loadingWindow.setLayout(new BorderLayout());
                loadingWindow.setSize(235, 235);
                loadingWindow.setBackground(Color.WHITE);
                JLabel loadingLabel = new JLabel(new javax.swing.ImageIcon(getClass().getResource("/org/sanggam/shambk/mpcs/resources/loading-big.gif")));
                loadingWindow.add(loadingLabel,BorderLayout.CENTER);
		loadingWindow.setLocation(Screen.xPosition(235), Screen.yPosition(235));
                loadingWindow.show(true);
    }

  
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

